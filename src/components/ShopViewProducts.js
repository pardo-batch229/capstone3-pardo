import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col, Form, Image } from 'react-bootstrap';
import {useParams, Navigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';
	
export default function ProductsView(){

	const {user} = useContext(UserContext);

	const{productId} = useParams();
	console.log(productId);

    const retrieveToken = localStorage.getItem('token');
    const [isAdmin, setIsAdmin] = useState(null);

	const [name, setName] = useState("");
    const [brand, setBrand] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [imgSource, setImageSource] = useState("");

	const [quantity, setQuantity] = useState('')
	const [total, setTotal] = useState('')


	useEffect(() => {
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
				.then(res => res.json())
				.then(data => {

					console.log(data);

					setName(data.name);
                    setBrand(data.brand)
					setDescription(data.description);
					setPrice(data.price);
					setImageSource(data.imgSource);

				});


	},[])

	const purchase = (productId) => {
		console.log(productId);
		fetch(`${ process.env.REACT_APP_API_URL }/users/checkOut`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						 Authorization: `Bearer ${retrieveToken}`
					},
					body: JSON.stringify({
						products:[{
							productId: productId,
							quantity: quantity
						}]

					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if(data){
						Swal.fire({
							title: "Successfully Purchased!",
							icon: 'success',
							text: `You have successfully ordered ${name}.`
						})

					}else{
						Swal.fire({
							title: "Something went wrong!",
							icon: 'error',
							text: "Please try again."
						})
					}

				});
	}

    /* User Verification */
   useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
        headers:{
            Authorization: `Bearer ${user.token}`
        }
    })
      .then(res => res.json())
      .then(data => {
        if (data.isAdmin) {
          setIsAdmin(true);
        } else {
          setIsAdmin(false);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

	return(
		(retrieveToken === null)?
			<Navigate to="/"/>
		:

    <Form onSubmit = {(e) => purchase(e)}>
    <Container className="mt-5">
    <Row>
        <Col lg={{ span: 6, offset: 3 }}>
            <Card>
                <Card.Body className="text-center">
					<Image className="mb-4"
					src={imgSource}
					/>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Brand:</Card.Subtitle>
                    <Card.Text>{brand}</Card.Text>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PHP {price}</Card.Text>
                    
                    {
						(user.token !== null)?
							(!isAdmin)?
							<>
							<Container fluid="sm">
								<Row>
									<Col>
										<Form.Group className ='mb-1' controlId='quantity'>
											<Form.Label>Quantity: </Form.Label>
											<Form.Control
											type = 'number'
											value={quantity}
											onChange = {e => setQuantity(e.target.value)}
											required/>
										</Form.Group>
									</Col>
									<Col>
										<Form.Group className ='mb-1' controlId='quantity'>
											<Form.Label>Total: </Form.Label>
											<Form.Control
											type = 'number'
											value={quantity*price}
											onChange = {e => setTotal(e.target.value)}
											required
											disabled/>
										</Form.Group>
									</Col>
								</Row>
										<Button variant="primary" onClick={() => purchase(productId)} block>Order</Button>
										<Link className= 'btn btn-success m-3'to='/shop' block> Back </Link>
								
							</Container>
							</>
							:
							<>
							<Link className ="btn btn-danger btn-block" to ="/login">Login</Link>
							<Link className= 'btn btn-success m-3'to='/shop'> Back </Link>
							</>
						:
						<>
						<Link className ="btn btn-danger btn-block" to ="/login">Login</Link>
						<Link className= 'btn btn-success m-3'to='/shop'> Back </Link>
						</>
					}                    
                </Card.Body>		
            </Card>
        </Col>
    </Row>
</Container>

    </Form>
	)
}
