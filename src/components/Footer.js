import React from "react";
import '../components/AppNavBarStyles.css'



const Footer = () => {
    return (
      <>
      <footer>
              <p>
                @{new Date().getFullYear()} All Rights Reserved.
              </p>
        </footer>
      </>
    );
  };

  export default Footer;