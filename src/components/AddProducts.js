import React, { useContext, useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

const AddProductModal = () => {
  const {user} = useContext(UserContext);
  const [show, setShow] = useState(false);
  const [product, setProduct] = useState({ name: '', brand: '', description: '', price: 0, stocks: 0 });

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleInputChange = (event) => {
    setProduct({
      ...product,
      [event.target.name]: event.target.value
    });
  };

  const handleSubmit = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
        method: 'POST',
        headers: {
           Authorization: `Bearer ${user.token}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(product)
      });
      const data = await response.json();
      console.log(data);
      handleClose();
      if(data === true){
        Swal.fire({
            title: "Successfully Added Product",
            icon: "success",
            text: "Product added"              
          })       
    }else{
        Swal.fire({
            title: "Opps! Something went wrong.",
            icon: "error",
            text: "Please try again"
        })
    }
    } catch (error) {
      if(error){
        Swal.fire({
          title: "UnAuthorized Access!",
          icon: "error",
          text: "Please try again"
      })
      }
        
    }
  };

  return (
    <>
    <div className='d-flex justify-content-end mb-2'>
        <Button variant="primary" onClick={handleShow}>
        Add Product
        </Button>
    </div>
      

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <label htmlFor="pname">Product Name</label>
              <input
                type="text"
                className="form-control"
                id="pname"
                name="name"
                value={product.name}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="pbrand">Product Brand</label>
              <input
                type="text"
                className="form-control"
                id="pbrand"
                name="brand"
                value={product.brand}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="description">Description</label>
              <input
                type="text"
                className="form-control"
                id="description"
                name="description"
                value={product.description}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="price">Price</label>
              <input
                type="number"
                className="form-control"
                id="price"
                name="price"
                value={product.price}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="stocks">Stocks</label>
              <input
                type="number"
                className="form-control"
                id="stocks"
                name="stocks"
                value={product.stocks}
                onChange={handleInputChange}
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSubmit}>
            Add
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default AddProductModal;
