import UserProfile from '../pages/userProfile';
import { useState, useEffect } from 'react';
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown';
import {NavLink} from 'react-router-dom'
import { useContext } from 'react';
import UserContext from '../UserContext';
import LoginModal from '../pages/LoginModal';
 
import './AppNavBarStyles.css'

export default function AppNavBar() {

   const {user} = useContext(UserContext)

   const [isActive, setActive] = useState(false);
   const [isAdmin, setIsAdmin] = useState(null);
   const retrieveToken = localStorage.getItem('token')


   const handleClick = () =>{
        setActive(!isActive)
   }

   /* User Verification */
   useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
        headers:{
            Authorization: `Bearer ${user.token}`
        }
    })
      .then(res => res.json())
      .then(data => {
        if (data.isAdmin) {
          setIsAdmin(true);
        } else {
          setIsAdmin(false);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  

  
  return (

    <>
    <Nav className='nav'>
        <Nav.Link className='p-1'><svg id="logo-35" width="50" height="41" viewBox="0 0 50 39" fill="none" xmlns="http://www.w3.org/2000/svg"> 
        <path d="M16.4992 2H37.5808L22.0816 24.9729H1L16.4992 2Z" className="ccompli1" fill="#007AFF"></path> 
        <path d="M17.4224 27.102L11.4192 36H33.5008L49 13.0271H32.7024L23.2064 27.102H17.4224Z" className="ccustom" fill="#312ECB"></path> 
        </svg></Nav.Link>
        
            <ul id='navbar' className={(isActive) ? '#navbar active' : '#navbar'}>
                {
                    //checks if the token is if null or not
                    (retrieveToken !== null)?
                        //checks if the user.isAdmin if true 
                        (isAdmin)? 
                        <>
                        <li><Nav.Link className='p-0' id="a"  as={NavLink} to="/admin">Home</Nav.Link></li>
                        <li><a id='a' href="/orders">Orders</a></li>
                        <li><a id='a' href="/usersDetail">Users</a></li>
                        <li><a id='a' href="/productsList">Products</a></li>
                            
                            <NavDropdown
                            id="nav-dropdown"
                            title={user.userName}
                            menuVariant="white"
                            >
                                <NavDropdown.Item><UserProfile/></NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.2">
                                    Change Password
                                </NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item as={NavLink} to='/logout'>
                                Logout
                                </NavDropdown.Item>
                            </NavDropdown> 
                            </>
                            :
                            
                            <>
                            <li><Nav.Link className='p-0' id="a" as={NavLink} to="/">Home</Nav.Link></li>
                            <li><Nav.Link className='p-0' id="a"  as={NavLink} to="/shop">Shop</Nav.Link></li>
                            
                            <NavDropdown
                            id="nav-dropdown"
                            title={user.userName}
                            menuVariant="white"
                            >   
                                <NavDropdown.Item as="div"><UserProfile/></NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.2">
                                    Change Password
                                </NavDropdown.Item>
                                <NavDropdown.Item href="/myOrders">My Order</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item as={NavLink} to='/logout'>
                                Logout
                                </NavDropdown.Item>
                            </NavDropdown>
                        </>
                         
                    :
                 <>        
                <li><Nav.Link className='p-0' id="a" as={NavLink} to="/">Home</Nav.Link></li>
                <li><Nav.Link className='p-0' id="a"  as={NavLink} to="/shop">Shop</Nav.Link></li>
                <li><LoginModal/></li>
                </>              
                }
                
            </ul>
        

        <div id='mobile' onClick={handleClick}>
            <i id='bar' className={(isActive) ? 'fas fa-times' : 'fas fa-bars' }></i>
        </div>
    </Nav>
    </>
  );

}


