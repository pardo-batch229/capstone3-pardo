
import React, { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';


export default function Highlights(){
    const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <Carousel activeIndex={index} onSelect={handleSelect}>
      <Carousel.Item> 
        <div className='w-70  d-flex flex-column justify-content-center align-items-center ' style={{backgroundImage: 'url(https://cf.shopee.ph/file/4df80444688b24f2e796cb59026e1cd9)', backgroundPosition: 'center center', backgroundSize: 'cover', backgroundRepeat: 'no-repeat', height: '50vh'  }}>
        </div>
      </Carousel.Item>
      <Carousel.Item>

        <div className='w-70 d-flex flex-column justify-content-center align-items-center ' style={{backgroundImage: 'url(https://cf.shopee.ph/file/6bc2fd9354237179ce7147e8445983e3)', backgroundPosition: 'center center', backgroundSize: 'cover', backgroundRepeat: 'no-repeat', height: '50vh'  }}>
        </div>
      </Carousel.Item>
      <Carousel.Item>

        <div className='w-70 d-flex flex-column justify-content-center align-items-center ' style={{backgroundImage: 'url(https://www.xds-computing.com/wp-content/uploads/2020/05/NO-BG-Lenovo-D330-10IGM.png)', backgroundPosition: 'center center', backgroundSize: 'cover', backgroundRepeat: 'no-repeat', height: '50vh'  }}>
        </div>

      </Carousel.Item>
    </Carousel>
  );
}