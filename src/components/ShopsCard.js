import {Card, Image} from 'react-bootstrap'
// import { useState } from 'react'
import { Link } from 'react-router-dom'

export default function ShopsCard({productsRetrieval}){
  // Checks to see if the data was successfully passed
  // console.log(courseProps)
  // console.log(typeof courseProps)

  // Destructuring the data to avoid dot notation
  const {name, brand, description, price, imgSource, _id} = productsRetrieval;

  // const [count, setCount] = useState(0)
  // console.log(useState(0))
  // const [availableSeat, setSeat] = useState(30)
 
    return(
      <Card className='mx-4 my-3 text-center'>
        <Card.Body>
        <Image className="mb-4"
        src={imgSource}
    />
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Brand:</Card.Subtitle>
            <Card.Text>
            {brand}
            </Card.Text>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>
            {description}
            </Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>
            PHP {price}
            </Card.Text>
                   
            {/* <Button onClick={enroll} variant="primary">Enroll</Button> */}
            <Link className='btn btn-primary' to={`/shopView/${_id}`}>Details</Link>
        </Card.Body>
      </Card>
        
    )
}