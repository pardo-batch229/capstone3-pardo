import './App.css';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext.js'
import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';
import Register from './pages/Register.js'
import Home from './pages/Home.js'
import ShopList from './pages/ShopList'; 
import Logout from './pages/Logout';
import ProductsList from './pages/ProductList';
import Error from './pages/Error';
import ShopView from './components/ShopViewProducts';
import Admin from './pages/Admin';
import UsersCredentials from './pages/UsersCredentials';
import Orders from './pages/Orders';
import UserOrders from './pages/userOrders';




function App() {
  const [user, setUser] = useState({
    /* 
      Syntax: 
        localStorage.getItem(propertyName)
        email: localStorage.getItem('email')
    */
    id: localStorage.getItem('token'),
    token: localStorage.getItem('token'),
    userName: localStorage.getItem('userName'),
    isAdmin: localStorage.getItem('isAdmin'),
  
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{

    console.log(user)
    console.log(localStorage)

  },[])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <>
        <Router>
        <AppNavBar/>
           <Container>
              <Routes>
                <Route exact path="/" element={<Home/>}></Route>
                
                <Route exact path="/register" element={<Register/>}></Route>
                <Route exact path="/shop" element={<ShopList/>}></Route>
                <Route exact path="/productsList" element={<ProductsList/>}></Route>
                <Route exact path="/shopView/:productId" element={<ShopView/>}></Route>
                <Route exact path="/logout" element={<Logout/>}></Route>
                <Route exact path="/orders" element={<Orders/>}></Route>
                <Route exact path="/usersDetail" element={<UsersCredentials/>}></Route>
                <Route exact path="/myOrders" element={<UserOrders/>}></Route>
                <Route exact path="/admin" element={<Admin/>}></Route>
                <Route exact path="*" element={<Error/>}></Route>
              </Routes> 
           </Container> 
          
        </Router>
    
    </>
    </UserProvider>
    
  );
}

export default App;
