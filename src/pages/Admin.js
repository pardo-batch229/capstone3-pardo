import Banner from '../components/Banner';

export default function Admin(){

    const data = {
        title: "Welcome back Admin",
        content: "Status Report",
        destination: "/admin",
        label: "back"
    }

    return(
        <Banner data={data}/>
    )
}
