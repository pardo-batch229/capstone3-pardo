import { useState, useEffect, useContext } from "react"
import { Table } from "react-bootstrap"
import { ClipLoader }  from 'react-spinners'
import UserContext from "../UserContext"


const UserOrders = () => {
	const { user } = useContext(UserContext)

	const [ orders, setOrders ] = useState([])
	const [loading, setLoading] = useState(false)
    

	const fetchOrders = async () => {
		setLoading(true)
		const response = await fetch (`${process.env.REACT_APP_API_URL}/users/myOrder`, {
			headers: {
				Authorization: `Bearer ${user.token}`,
			    'Content-Type': 'application/json'
			}
		})

		const data = await response.json()

        console.log(data)
		setOrders(data.map(order =>{
            return(
                <>
                    <tr key={order._id}>
                        <td className="text-center">{order._id}</td>
						<td className="text-center">{order.userName}</td>
                        <td className="text-center">{order.products[0].quantity}</td>
                        <td className="text-center">{order.totalAmount}</td>
                        <td className="text-center">
                            {
                                new Date(order.purchasedOn).toLocaleString("en-US", {
                                       year: "numeric",
                                      month: "short",
                                      day: "2-digit",
                                      hour: "2-digit",
                                      minute: "2-digit",
                                      hour12: true
                                })
                            }
                        </td>
                    </tr>
                    </>
            )
        }))
		setLoading(false)
	}

    useEffect(()=>{
        fetchOrders()
    },[])
	    

	return (
		<div className="">
			<div className="">
				<div className="w-100 mb-3 text-center mt-3">
					<h1>Orders</h1>
				</div>

				{ loading ? 
					<div><ClipLoader /></div>
					:
					<Table striped bordered hover>
			      <thead>
			        <tr>
			          <th className="text-center">Order Id</th>
                      <th className="text-center">User Name</th>
					  <th className="text-center">Quantity</th>
			          <th className="text-center">Total Amount</th>
			          <th className="text-center">Purhcased On</th>
			        </tr>
			      </thead>
			      <tbody>
                     {orders}
			      </tbody>
			    </Table>
				}
			</div>
		</div>
	)
}

export default UserOrders
