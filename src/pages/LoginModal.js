import React, { useState, useEffect, useContext  } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import UserContext from '../UserContext';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Admin from './Admin';




function LoginModal() {
    const [show, setShow] = useState(false);
    const [isActive, setIsActive] = useState(false)
    const [isAdmin, setIsAdmin] = useState(null)
    const retrievedToken = localStorage.getItem('token')

    const {user, setUser} = useContext(UserContext)
    const {unsetUser} = useContext(UserContext);
    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const navigate = useNavigate()


    const handleOpenModal = () => {
        setShow(true);
      };
    
      const handleCloseModal = () => {
        setShow(false);
      };


      useEffect(()=>{
        // Validation to enable register button when all fields are populated and both password match
        if(userName !== '' && password !== ''){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    },[userName, password])

    //fetching login
    function loginUser(e){
        // Prevents from reloading pages
        e.preventDefault()

       fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userName: userName,
                password: password
            })
       })
       .then(res => res.json())
       .then(data => {
        console.log(data)
        // If no user information is found, the "access" property will not be available and will return undefined
        // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
        if(typeof data.access !== "undefined"){
            localStorage.setItem('token', data.access)
            retrieveUserDetails(data.access)

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: `Welcome`
                })
                         
        }else{
            Swal.fire({
                title: "Authentication Failed",
                icon: "error",
                text: "Check your login credentials"
            })
            unsetUser()
            navigate('/')  
        }

       })

       const retrieveUserDetails = async (token) => {
        // The token will be sent as part of the request's header information
        // We put "Bearer" in front of the token to follow implementation standards for JWTs

       await fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(res => res.json())
        .then(data => {
            console.log(data)
            
            localStorage.setItem('userName', data.userName)
            localStorage.setItem('isAdmin', data.isAdmin)
            localStorage.setItem('id', data._id)
            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
            setUser({
                userName: data.userName,
                isAdmin: data.isAdmin
            })

            if(data.isAdmin){
                setIsAdmin(true)
            }else{
                setIsAdmin(false)
            }
        })
    }
    
    }
  
    return (
      <>
       
        <Button variant="primary" onClick={handleOpenModal}>
          Login
        </Button>
        {
        (retrievedToken !== null) ? 
            (isAdmin) ? <Admin/> : <Navigate to="/shop"/>
        :   
        <Modal show={show} onHide={handleCloseModal} centered>
          <Modal.Header closeButton>
            <Modal.Title className='text-center'>Login</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={(e)=>loginUser(e)}>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>User Name</Form.Label>
                <Form.Control type="userName" placeholder="User Name" value={userName} onChange={event => setUserName(event.target.value)} required/>
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlTextarea1"
              >
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)} required/>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Don't have an account yet? Register <a href='/register'>here</a></Form.Label>
            </Form.Group>
            {
                (isActive) ?
                <Button variant="primary" type="submit" controlid="submitBtn">
                    Login
                </Button>
                :
                <Button variant="primary" type="submit" controlid="submitBtn" disabled>
                    Login
                </Button>
            }
            </Form>
          </Modal.Body>
        </Modal>
        }
        
      </>
    );
  }
  
 export default LoginModal;