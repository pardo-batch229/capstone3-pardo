import { useContext, useState, useEffect } from "react"
import {Table, Button} from 'react-bootstrap'
import { Navigate } from "react-router-dom";
import Error from "./Error";
import UserContext from "../UserContext";
import Swal from "sweetalert2";


export default function UsersCredentials(){
    
    const [details, setDetails] = useState([])

    const [isAdmin, setIsAdmin] = useState(null);
    const {user} = useContext(UserContext);
    
    const fetchData = () =>{
        const currentUserId = localStorage.getItem("userId");
		fetch(`${process.env.REACT_APP_API_URL}/users/usersCredentials`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

            setDetails(data.filter(user => user._id !== currentUserId).map(users =>{
                
                    return (
                        <>
                        <tr key={users._id}>
                            <td>{users._id}</td>
                            <td>{users.firstName}</td>
                            <td>{users.lastName}</td>
                            <td>{users.mobileNumber}</td>
                            <td>{users.email}</td>
                            <td>{users.isAdmin ? "Admin" : "User"}</td>
                            {<td>
                                {(!users.isAdmin)
                                ?
                                <Button variant="primary" size="sm" onClick={() => setAdmin(users._id, users.userName)}>Set</Button>
                                :
                                <Button variant="success" className="mx-1" size="sm" onClick={() => unsetAdmin(users._id, users.userName)}>unSet</Button>
                                
                               }
                                </td>}
                        </tr>
                        </>
                    )
                
            }));
		});
	}
    
    useEffect(()=>{
        fetchData()
    },[])

    /* Set Admin */
    const setAdmin = (id, name) =>{
        console.log(id);
        
        fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin/${id}`,
        {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${user.token}`
            },
        body: JSON.stringify({
            isAdmin: true
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Successfully set an Admin",
                    icon: "success",
                    text: `${name} is now an Admin`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

    const unsetAdmin = (id, name) =>{
        console.log(id);
        
        fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin/${id}`,
        {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${user.token}`
            },
        body: JSON.stringify({
            isAdmin: false
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Successfully unset a User",
                    icon: "success",
                    text: `${name} is now a User`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

      
    /* User Verification */
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
            headers:{
                Authorization: `Bearer ${user.token}`
            }
        })
          .then(res => res.json())
          .then(data => {
            if (data.isAdmin) {
              setIsAdmin(true);
              fetchData()
            } else {
              setIsAdmin(false);
              fetchData()
            }
          })
          .catch(error => {
            console.error(error);
          });
      }, []);
      if(isAdmin === null){
         return (<div className="loading align-text-center">
                    <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                    </div>
                </div>
         )
      }
      if(!isAdmin){
         return <Error/>
      }

    
    return(
        <>
        {
            (user.isAdmin) ?
                <>
                <h1 className='text-center mt-2'>Users</h1>
                <Table striped bordered hover className="shadow-lg shadow-sm ">
                    <thead className="banner-bg">
                        <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Mobile Number</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {details} 
                    </tbody>
                    </Table>
                </>
            :
            <Navigate to="/"></Navigate>
                   
        }
        </>
    )
}
      
  
      