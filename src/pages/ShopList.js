import ShopsCard from "../components/ShopsCard.js"
import { useEffect, useState, useContext } from "react"
import UserContext from "../UserContext.js"
import Admin from "./Admin.js"

export default function ShopList(){

    const {user} = useContext(UserContext)
    const [products, setProducts] = useState([])

    const [isAdmin, setIsAdmin] = useState(null)

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`)
			        .then(res => res.json())
			        .then(data => {
			            
			            console.log(data);

                        setProducts(data.map(product=>{
                            return(
                                <ShopsCard key={product._id} productsRetrieval={product}/> 
                            )
                        }))
			        })
    },[])
    
     /* User Verification */
   useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
        headers:{
            Authorization: `Bearer ${user.token}`
        }
    })
      .then(res => res.json())
      .then(data => {
        if (data.isAdmin) {
          setIsAdmin(true);
        } else {
          setIsAdmin(false);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }, []);
    
    return(
        <>
        {
            (!isAdmin)?
            <>
            <h1 className='mt-3'>Product</h1>
            {products}
            </>
            :
            <Admin/>
        }
        </>
    )   
}
