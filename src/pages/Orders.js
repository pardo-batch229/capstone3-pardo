import { useState, useEffect, useContext } from "react"
import { Table } from "react-bootstrap"
import { ClipLoader }  from 'react-spinners'
import UserContext from "../UserContext"
import Error from "./Error"

const Orders = () => {
	const { user } = useContext(UserContext)
	const [ orders, setOrders ] = useState([])
	const [loading, setLoading] = useState(false)
    const [isAdmin, setIsAdmin] = useState(null);

	const fetchOrders = async () => {
		setLoading(true)
		const response = await fetch (`${process.env.REACT_APP_API_URL}/users/allOrders`, {
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()

		setOrders(data)
		setLoading(false)
	}

	useEffect(() => {
		fetchOrders()
	}, [])

    /* User Verification */
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
            headers:{
                Authorization: `Bearer ${user.token}`
            }
        })
          .then(res => res.json())
          .then(data => {
            if (data.isAdmin) {
              setIsAdmin(true);
            } else {
              setIsAdmin(false);
            }
          })
          .catch(error => {
            console.error(error);
          });
      }, []);

      if(isAdmin === null){
         return (<div className="loading align-text-center">
                    <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                    </div>
                </div>
         )
      }
    
      if(!isAdmin){
         return <Error/>
      }

	return (
		<div className="">
			<div className="">
				<div className="w-100 mb-3 text-center mt-3">
					<h1>Orders</h1>
				</div>

				{ loading ? 
					<div><ClipLoader /></div>
					:
					<Table striped bordered hover>
			      <thead>
			        <tr>
			          <th className="text-center">Order Id</th>
			          <th className="text-center">User Id</th>
                      <th className="text-center">User Name</th>
					  <th className="text-center">Product Name</th>
					  <th className="text-center">Quantity</th>
			          <th className="text-center">Total Amount</th>
			          <th className="text-center">Purhcased On</th>
			        </tr>
			      </thead>
			      <tbody>
			        {orders.map(order => (
			        	<tr key={order._id}>
			        		<td className="text-center">{order._id}</td>
			        		<td className="text-center">{order.userId}</td>
                            <td className="text-center">{order.userName}</td>
							<td className="text-center">{order.products[0].productName}</td>
							<td className="text-center">{order.products[0].quantity}</td>
			        		<td className="text-center">{order.totalAmount}</td>
			        		<td className="text-center">
			        			{
			        				new Date(order.purchasedOn).toLocaleString("en-US", {
		        					 	  year: "numeric",
									      month: "short",
									      day: "2-digit",
									      hour: "2-digit",
										  minute: "2-digit",
										  hour12: true
			        				})
			        			}
			        		</td>
			        	</tr>
			        ))}
			      </tbody>
			    </Table>
				}
			</div>
		</div>
	)
}

export default Orders
