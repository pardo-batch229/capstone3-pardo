import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

const data ={
    title: "Digital Store",
    content: "Conveniency for everyone, shop everywhere!",
    destination: "/shop",
    label: "Shop Now!"
}

export default function Home(){
    return(
        <>
            <Banner data={data}/>
            <Highlights/>
        </>
    )
}