import { useEffect, useState, useContext } from "react"
import UserContext from "../UserContext"
import { Modal, Button, Form } from "react-bootstrap"


export default function UserProfile(){

  const [show, setShow] = useState(false);
  const {user} = useContext(UserContext);

  const [userName, setUserName] = useState('Loading...');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNumber, setMobileNumber] = useState(0);

  // Modals function
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    if (user.token) {
      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(`Users: ${data}`)
          setUserName(data.userName);
          setFirstName(data.firstName);
          setLastName(data.lastName);
          setEmail(data.email);
          setMobileNumber(data.mobileNumber);
          
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, []);

    return(
        <>
        <span onClick={handleShow} style={{ cursor: "pointer" }}>
            Profile
        </span>

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
            <Modal.Title>Profile</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Form.Group controlId="userName" className="mb-3">
                <Form.Label>User Name: </Form.Label>
                <Form.Control type="text" value={userName} disabled />
            </Form.Group>

            <Form.Group controlId="userName" className="mb-3">
                <Form.Label>First Name: </Form.Label>
                <Form.Control type="text" value={firstName} disabled />
            </Form.Group>

            <Form.Group controlId="userName" className="mb-3">
                <Form.Label>Last Name: </Form.Label>
                <Form.Control type="text" value={lastName} disabled />
            </Form.Group>

            <Form.Group controlId="userName" className="mb-3">
                <Form.Label>Email: </Form.Label>
                <Form.Control type="text" value={email} disabled />
            </Form.Group>

            <Form.Group controlId="userName" className="mb-3">
                <Form.Label>Mobile Number: </Form.Label>
                <Form.Control type="text" value={mobileNumber} disabled />
            </Form.Group>

            </Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
                Close
            </Button>
            </Modal.Footer>
        </Modal>
    </>
    )
}

