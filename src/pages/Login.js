import {Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {useNavigate, Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from "sweetalert2"
import Admin from './Admin';

export default function Login(){

    // Allow us to consume the User Context object and its properties
    const {user, setUser} = useContext(UserContext)
    const {unsetUser} = useContext(UserContext);
    const navigate = useNavigate()
    const retrieveToken = localStorage.getItem('token');
    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)

    useEffect(()=>{
        // Validation to enable register button when all fields are populated and both password match
        if(userName !== '' && password !== ''){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    },[userName, password])
    // Function to simulate user registration

    function loginUser(e){
        // Prevents from reloading pages

       fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userName: userName,
                password: password
            })
       })
       .then(res => res.json())
       .then(data => {
        console.log(data)
        // If no user information is found, the "access" property will not be available and will return undefined
        // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
        if(typeof data.access !== "undefined"){
            localStorage.setItem('token', data.access)
            retrieveUserDetails(data.access)

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: `Welcome`
                })
                         
        }else{
            Swal.fire({
                title: "Authentication Failed",
                icon: "error",
                text: "Check your login credentials"
            })
            unsetUser()
            navigate('/login')  
        }

       })

    
    }

    const retrieveUserDetails = async (token) => {
        // The token will be sent as part of the request's header information
        // We put "Bearer" in front of the token to follow implementation standards for JWTs

       await fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then(res => res.json())
        .then(data => {
            console.log(data)
            
            localStorage.setItem('userName', data.userName)
            localStorage.setItem('isAdmin', data.isAdmin)
            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
            setUser({
                userName: data.userName,
                isAdmin: data.isAdmin
            })
        })
    }
   
    return(
        
         (retrieveToken !== null) ? 
            (user.isAdmin) ? <Admin/> : <Navigate to="/shop"/>
        :            
        <Form onSubmit={(e)=>loginUser(e)}>
        <h1 className='mt-3'>LOGIN</h1>
            <Form.Group className="mb-3" controlid="userName">
                <Form.Label>User Name</Form.Label>
                <Form.Control type="userName" placeholder="User Name" value={userName} onChange={event => setUserName(event.target.value)} required/>
            </Form.Group>

            <Form.Group className="mb-3" controlid="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)} required/>
            </Form.Group>
            <Form.Group className="mb-3">
            <Form.Label>Don't have an account yet? Register <a href='/register'>here</a></Form.Label>
            </Form.Group>

            {
                (isActive) ?
                <Button variant="success" type="submit" controlid="submitBtn">
                    Login
                </Button>
                :
                <Button variant="primary" type="submit" controlid="submitBtn" disabled>
                    Login
                </Button>
            }
    </Form>
    )
}