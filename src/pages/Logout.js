import { useContext, useEffect } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";


export default function Logout(){
    
    // Consume the UserContext object and destructure it to access the user state and unsetUser function from context provider
    const {unsetUser, setUser} = useContext(UserContext);

    // clear the localstorage of the user's information
    unsetUser()

    // localStorage.clear(); 

    useEffect(()=>{

        // Set the user state back to its original value
        setUser({token:null, id: null, isAdmin:null})
    })

    return(
            <Navigate to="/"/>
        )
}